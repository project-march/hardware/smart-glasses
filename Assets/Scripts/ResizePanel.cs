using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ResizePanel : MonoBehaviour
{
    public string gaitName;
    public string menuName;

    public GameObject currentMenu;
    private GameObject[] currentPanels;

    // Start is called before the first frame update
    void Start()
    {
        currentMenu = GameObject.Find("StartMenu");
        currentMenu.SetActive(true);
        ChangeActiveGait();
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            ChangeActiveGait();
        }
    }

    void ChangeActiveGait()
    {
        // Change the menu if the selected gait is in another one
        if (currentMenu.name != menuName)
        {
            print("Detecting a different menu");
            // Turn off every panel in the old menu
            foreach (Transform child in currentMenu.transform)
            {
                child.gameObject.SetActive(false);
            }

            // Set the new menu
            currentMenu = GameObject.Find(menuName);

            // Turn on every panel in the new menu
            foreach (Transform child in currentMenu.transform)
            {
                child.gameObject.SetActive(true);
            }
        }
        
        // Size the panels if they are needed
        currentPanels = GameObject.FindGameObjectsWithTag(menuName);
        foreach (GameObject Panl in currentPanels)
        {
            if (Panl.name == gaitName)
            {
                Panl.GetComponent<Animator>().Play("BlockScale");
            }

            else
            {
                if (Panl.GetComponent<Animator>().GetCurrentAnimatorStateInfo(0).IsName("BlockScale"))
                {
                    Panl.GetComponent<Animator>().Play("BlockShrink");
                }
            }
        }
    }
}
