using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CloneImage : MonoBehaviour
{
    public string calledGait;
    private Sprite gaitImage;
    public GameObject displayer;

    // Start is called before the first frame update
    void Start()
    {
        gaitImage = Resources.Load<Sprite>("TestSprite");
        displayer.GetComponent<Image>().sprite = gaitImage;
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
